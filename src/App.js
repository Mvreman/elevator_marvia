import React from 'react';
import Button from './components/Button';
import Floors from './components/Floors';
import Elevator from './components/Elevator/elevator';
import './App.css';
import ElevatorData from "./redux/data/data";
import { Row, Col, Card} from 'react-bootstrap';

const App = () => {

  return (
      <div>
          <div className="header">
              <h2>Elevator</h2>
          </div>
          <div className="container">
              <Row>
                  <Col >
                      <div className="control-panel">
                          <h2 className="cp_title">Control panel</h2>
                          <div className="button-container">
                          {ElevatorData.map((elevator, nbr) => (
                                  <Button key={nbr} name={elevator.name} value={elevator.name}/>
                              ))}
                          </div>
                      </div>
                  </Col>
                  <Col>
                    <div className="container-elevator">
                      <div className="building">
                          {ElevatorData.map((container, nbr) => (
                                  <Floors />))}
                          <Elevator/>
                      </div>
                    </div>
                  </Col>
                  <Col>
                      <div>
                          {/*<Card>*/}
                          {/*    {ElevatorData.map((elevator) => (*/}
                          {/*      <p>{elevator.title} </p>))}*/}

                          {/*  </Card>*/}
                        </div>
                  </Col>
              </Row>
          </div>
      </div>
  )
};

export default App;
