//the data of the elevator
const ElevatorData = [
  {
    name: 9,
    title: 'Welkom bij verdieping 9',
    description: 'test'
  },
  {
    name: 8,
    title: 'Welkom bij verdieping 8'
  },
  {
    name: 7,
    title: 'Welkom bij verdieping 7'
  },
  {
    name: 6,
    title: 'Welkom bij verdieping 6'
  },
  {
    name: 5,
    title: 'Welkom bij verdieping 5'
  },
  {
    name: 4,
    title: 'Welkom bij verdieping 4'
  },
  {
    name: 3,
    title: 'Welkom bij verdieping 3'
  },
  {
    name: 2,
    title: 'Welkom bij verdieping 2'
  },
  {
    name: 1,
    title: 'Welkom bij verdieping 1'
  },
  {
    name: 0,
    title: 'Welkom bij op de begaande grond'
  }
];

export default ElevatorData;
